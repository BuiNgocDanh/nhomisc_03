1. Satisfy the customer:
   Ưu tiên cao nhất của chúng tôi là thỏa mãn khách hàng thông qua việc chuyển giao sớm và liên tục các phần mềm có giá trị.
2. Welcome Change
   Chào đón việc thay đổi các yêu cầu, thậm chí rất muộn, trong quá trình phát triển. Các quy trình Agile khai thác sự thay đổi cho các      lợi thế cạnh tranh của khách hàng.
9. Continuous attention to technical:
    Lien tuc quan tam den cac ki thuat và thiet ke tot de gia tang su linh hoat.
10. Simplicity is essential:
    Su don gian – nghe thuat toi da hoa luong cong viec chua xong – la can ban.
11. Self-Organizing teams
    Các kiến trúc tốt nhất, yêu cầu tốt nhất, và thiết kế tốt nhất sẽ được làm ra bởi các nhóm tự tổ chức.
12. Reflect and adjust.
    Nhóm phát triển sẽ thường xuyên suy nghĩ về việc làm sao để trở nên hiệu quả hơn, sau đó họ sẽ điều chỉnh và thay đổi các hành vi         của mình cho phù hợp.
	
